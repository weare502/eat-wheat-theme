/* jshint devel: true */
/* global FWP */
/* jshint ignore:start */
/**
 * String.startsWith shim
 */
if (!String.prototype.startsWith) {
		String.prototype.startsWith = function(searchString, position){
			position = position || 0;
			return this.substr(position, searchString.length) === searchString;
	};
}

/**
 * FitVids JS
 */
;(function( $ ){

	'use strict';

	$.fn.fitVids = function( options ) {
		var settings = {
			customSelector: null,
			ignore: null
		};

		if(!document.getElementById('fit-vids-style')) {
			// appendStyles: https://github.com/toddmotto/fluidvids/blob/master/dist/fluidvids.js
			var head = document.head || document.getElementsByTagName('head')[0];
			var css = '.fluid-width-video-wrapper{width:100%;position:relative;padding:0;}.fluid-width-video-wrapper iframe,.fluid-width-video-wrapper object,.fluid-width-video-wrapper embed {position:absolute;top:0;left:0;width:100%;height:100%;}';
			var div = document.createElement("div");
			div.innerHTML = '<p>x</p><style id="fit-vids-style">' + css + '</style>';
			head.appendChild(div.childNodes[1]);
		}

		if ( options ) {
			$.extend( settings, options );
		}

		return this.each(function(){
			var selectors = [
				'iframe[src*="player.vimeo.com"]',
				'iframe[src*="youtube.com"]',
				'iframe[src*="youtube-nocookie.com"]',
				'iframe[src*="kickstarter.com"][src*="video.html"]',
				'object',
				'embed'
			];

			if (settings.customSelector) {
				selectors.push(settings.customSelector);
			}

			var ignoreList = '.fitvidsignore';

			if(settings.ignore) {
				ignoreList = ignoreList + ', ' + settings.ignore;
			}

			var $allVideos = $(this).find(selectors.join(','));
			$allVideos = $allVideos.not('object object'); // SwfObj conflict patch
			$allVideos = $allVideos.not(ignoreList); // Disable FitVids on this video.

			$allVideos.each(function(){
				var $this = $(this);
				if($this.parents(ignoreList).length > 0) {
					return; // Disable FitVids on this video.
				}
				if (this.tagName.toLowerCase() === 'embed' && $this.parent('object').length || $this.parent('.fluid-width-video-wrapper').length) { return; }
				if ((!$this.css('height') && !$this.css('width')) && (isNaN($this.attr('height')) || isNaN($this.attr('width'))))
				{
					$this.attr('height', 9);
					$this.attr('width', 16);
				}
				var height = ( this.tagName.toLowerCase() === 'object' || ($this.attr('height') && !isNaN(parseInt($this.attr('height'), 10))) ) ? parseInt($this.attr('height'), 10) : $this.height(),
						width = !isNaN(parseInt($this.attr('width'), 10)) ? parseInt($this.attr('width'), 10) : $this.width(),
						aspectRatio = height / width;
				if(!$this.attr('name')){
					var videoName = 'fitvid' + $.fn.fitVids._count;
					$this.attr('name', videoName);
					$.fn.fitVids._count++;
				}
				$this.wrap('<div class="fluid-width-video-wrapper"></div>').parent('.fluid-width-video-wrapper').css('padding-top', (aspectRatio * 100)+'%');
				$this.removeAttr('height').removeAttr('width');
			});
		});
	};
	
	// Internal counter for unique video names.
	$.fn.fitVids._count = 0;
	
// Works with either jQuery or Zepto
})( window.jQuery || window.Zepto );
/* jshint ignore:end */


var freezeVp = function(e) {
    e.preventDefault();
};

function stopBodyScrolling (bool) {
    if (bool === true) {
        document.body.addEventListener("touchmove", freezeVp, false);
    } else {
        document.body.removeEventListener("touchmove", freezeVp, false);
    }
}


jQuery( document ).ready( function( $ ) {
	var $body = $('body');
	$body.fitVids();
	
	// window.eatwheatPreviousPage = 0;

	// $(document).on('facetwp-loaded', function(){
	// 	if ( FWP.settings.pager !== undefined && FWP.settings.pager.page > 1 ){
	// 		console.log( FWP.settings.pager.page );
	// 		$('body').removeClass('paged-' + window.eatwheatPreviousPage );
	// 		$('body').addClass('paged-' + FWP.settings.pager.page );
	// 		window.eatwheatPreviousPage = FWP.settings.pager.page;
	// 	}
	// });
	
	$(document).on('facetwp-refresh', function(){
		console.log(parseInt(FWP.paged, 10));
		if ( FWP.paged !== undefined && parseInt(FWP.paged, 10) > 1 ){
			$('body').addClass('paged');
		}
	});

	$('.searchform input[name="s"]').attr('placeholder', 'Search for anything...');

	$(document).on('facetwp-loaded', function() {
		if (window.facetWPhasloaded){
			$('html, body').animate({ scrollTop: ($('#content').offset().top) }, 500);
		}

		window.facetWPhasloaded = true;
     });

	$('.open-popup-link').magnificPopup({
		type:'inline',
		removalDelay: 300,
		midClick: true // Allow opening popup on middle mouse click. Always set it to true if you don't provide alternative source in href.
	});
	
	$('.magnific-trigger').magnificPopup({
		type:'inline',
		removalDelay: 300,
		midClick: true
	});

	$('#fullscreen-recipe').magnificPopup({
		type:'inline',
		removalDelay: 300,
		midClick: true,
		callbacks: {
			open: function(){
				$('html').addClass('no-scroll');
				$('body').addClass('no-scroll');

				stopBodyScrolling(true);

				$('.recipe-slider').slick({
					prevArrow: '#recipe-prev',
					nextArrow: '#recipe-next',
					infinite: false
				});
				var totalSlides = $('.recipe-fullscreen-content .recipe-step').length;
				$('#totalRecipeSteps').text(totalSlides);
				$('.recipe-slider').on('afterChange', function(event, slick, currentSlide){
					$('#currentRecipeStep').text(currentSlide + 1);
					if (currentSlide > 0){
						$('#recipe-prev').removeClass('hidden-button');
					} else {
						$('#recipe-prev').addClass('hidden-button');
					}
					if ( currentSlide === ( totalSlides - 1) ){
						$('#recipe-next').addClass('hidden-button');
					} else {
						$('#recipe-next').removeClass('hidden-button');
					}
				});
			},
			close: function(){
				$('html').removeClass('no-scroll');
				$('body').removeClass('no-scroll');

				stopBodyScrolling(false);

			}
		}
	});

	$(function() {
		if ( ! window.HomeVideoID ) {
			return;
		}

		$('body.home').ContainerPlayer({
			overlay: { color: 'rgba(0,0,0,0.4)' },
			vimeo: {  
				tranitionIn: true,
				videoId: window.HomeVideoID,
				poster: window.HomeVideoPoster,
			},
			/*youTube: {  
				videoId: 'quG1iBo8Gc8',
				poster: '../test/background2.jpg',
			},*/
		}).on('video.playing video.paused video.loaded video.ended', function(e) {
			console.log(e);
		}).on('video.loaded', function(){
			$('body').addClass('playing-video');
		});
	});
}); // End Document Ready