/* jshint devel: true */
/* global FWP */
/* jshint ignore:start */
/**
 * String.startsWith shim
 */
function stopBodyScrolling(e){!0===e?document.body.addEventListener("touchmove",freezeVp,!1):document.body.removeEventListener("touchmove",freezeVp,!1)}String.prototype.startsWith||(String.prototype.startsWith=function(e,t){return t=t||0,this.substr(t,e.length)===e})
/**
 * FitVids JS
 */,function(d){"use strict";d.fn.fitVids=function(e){var i={customSelector:null,ignore:null};if(!document.getElementById("fit-vids-style")){
// appendStyles: https://github.com/toddmotto/fluidvids/blob/master/dist/fluidvids.js
var t=document.head||document.getElementsByTagName("head")[0],o=".fluid-width-video-wrapper{width:100%;position:relative;padding:0;}.fluid-width-video-wrapper iframe,.fluid-width-video-wrapper object,.fluid-width-video-wrapper embed {position:absolute;top:0;left:0;width:100%;height:100%;}",n=document.createElement("div");n.innerHTML='<p>x</p><style id="fit-vids-style">'+o+"</style>",t.appendChild(n.childNodes[1])}return e&&d.extend(i,e),this.each(function(){var e=['iframe[src*="player.vimeo.com"]','iframe[src*="youtube.com"]','iframe[src*="youtube-nocookie.com"]','iframe[src*="kickstarter.com"][src*="video.html"]',"object","embed"];i.customSelector&&e.push(i.customSelector);var r=".fitvidsignore";i.ignore&&(r=r+", "+i.ignore);var t=d(this).find(e.join(","));// Disable FitVids on this video.
(// SwfObj conflict patch
t=(t=t.not("object object")).not(r)).each(function(){var e=d(this);if(!(0<e.parents(r).length||"embed"===this.tagName.toLowerCase()&&e.parent("object").length||e.parent(".fluid-width-video-wrapper").length)){e.css("height")||e.css("width")||!isNaN(e.attr("height"))&&!isNaN(e.attr("width"))||(e.attr("height",9),e.attr("width",16));var t,i,o=("object"===this.tagName.toLowerCase()||e.attr("height")&&!isNaN(parseInt(e.attr("height"),10))?parseInt(e.attr("height"),10):e.height())/(isNaN(parseInt(e.attr("width"),10))?e.width():parseInt(e.attr("width"),10));if(!e.attr("name")){var n="fitvid"+d.fn.fitVids._count;e.attr("name",n),d.fn.fitVids._count++}e.wrap('<div class="fluid-width-video-wrapper"></div>').parent(".fluid-width-video-wrapper").css("padding-top",100*o+"%"),e.removeAttr("height").removeAttr("width")}})})},
// Internal counter for unique video names.
d.fn.fitVids._count=0}(window.jQuery||window.Zepto);
/* jshint ignore:end */
var freezeVp=function(e){e.preventDefault()};jQuery(document).ready(function(n){var e;n("body").fitVids(),
// window.eatwheatPreviousPage = 0;
// $(document).on('facetwp-loaded', function(){
// 	if ( FWP.settings.pager !== undefined && FWP.settings.pager.page > 1 ){
// 		console.log( FWP.settings.pager.page );
// 		$('body').removeClass('paged-' + window.eatwheatPreviousPage );
// 		$('body').addClass('paged-' + FWP.settings.pager.page );
// 		window.eatwheatPreviousPage = FWP.settings.pager.page;
// 	}
// });
n(document).on("facetwp-refresh",function(){console.log(parseInt(FWP.paged,10)),void 0!==FWP.paged&&1<parseInt(FWP.paged,10)&&n("body").addClass("paged")}),n('.searchform input[name="s"]').attr("placeholder","Search for anything..."),n(document).on("facetwp-loaded",function(){window.facetWPhasloaded&&n("html, body").animate({scrollTop:n("#content").offset().top},500),window.facetWPhasloaded=!0}),n(".open-popup-link").magnificPopup({type:"inline",removalDelay:300,midClick:!0}),n(".magnific-trigger").magnificPopup({type:"inline",removalDelay:300,midClick:!0}),n("#fullscreen-recipe").magnificPopup({type:"inline",removalDelay:300,midClick:!0,callbacks:{open:function(){n("html").addClass("no-scroll"),n("body").addClass("no-scroll"),stopBodyScrolling(!0),n(".recipe-slider").slick({prevArrow:"#recipe-prev",nextArrow:"#recipe-next",infinite:!1});var o=n(".recipe-fullscreen-content .recipe-step").length;n("#totalRecipeSteps").text(o),n(".recipe-slider").on("afterChange",function(e,t,i){n("#currentRecipeStep").text(i+1),0<i?n("#recipe-prev").removeClass("hidden-button"):n("#recipe-prev").addClass("hidden-button"),i===o-1?n("#recipe-next").addClass("hidden-button"):n("#recipe-next").removeClass("hidden-button")})},close:function(){n("html").removeClass("no-scroll"),n("body").removeClass("no-scroll"),stopBodyScrolling(!1)}}}),n(function(){window.HomeVideoID&&n("body.home").ContainerPlayer({overlay:{color:"rgba(0,0,0,0.4)"},vimeo:{tranitionIn:!0,videoId:window.HomeVideoID,poster:window.HomeVideoPoster}}).on("video.playing video.paused video.loaded video.ended",function(e){console.log(e)}).on("video.loaded",function(){n("body").addClass("playing-video")})})});// End Document Ready