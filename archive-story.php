<?php
/**
 * Temoplate for showing all recipes
 *
 * @package  WordPress
 * @subpackage  Timber
 * @since    Timber 0.1
 */

$context = Timber::get_context();
$post = new TimberPost();
$post->thumbnail = $post->get_thumbnail();
$context['post'] = $post;
$context['posts'] = new Timber\PostQuery();
$context['pagination'] = Timber::get_pagination();
$context['header_bg_image'] = get_field('story_archive_header_image', 'options');

Timber::render( 'stories-archive.twig', $context );