<?php
$labels = array(
	'name'                => __( 'Stories', 'eat-wheat' ),
	'singular_name'       => __( 'Story', 'eat-wheat' ),
	'add_new'             => _x( 'Add New Story', 'eat-wheat', 'eat-wheat' ),
	'add_new_item'        => __( 'Add New Story', 'eat-wheat' ),
	'edit_item'           => __( 'Edit Story', 'eat-wheat' ),
	'new_item'            => __( 'New Story', 'eat-wheat' ),
	'view_item'           => __( 'View Story', 'eat-wheat' ),
	'search_items'        => __( 'Search Stories', 'eat-wheat' ),
	'not_found'           => __( 'No Stories found', 'eat-wheat' ),
	'not_found_in_trash'  => __( 'No Stories found in Trash', 'eat-wheat' ),
	'parent_item_colon'   => __( 'Parent Story:', 'eat-wheat' ),
	'menu_name'           => __( 'Stories', 'eat-wheat' ),
);

$args = array(
	'labels'              => $labels,
	'hierarchical'        => false,
	'taxonomies'          => array(),
	'public'              => true,
	'show_ui'             => true,
	'show_in_menu'        => true,
	'show_in_admin_bar'   => true,
	'menu_position'       => null,
	'menu_icon'           => 'dashicons-book',
	'show_in_nav_menus'   => true,
	'publicly_queryable'  => true,
	'exclude_from_search' => false,
	'has_archive'         => true,
	'query_var'           => true,
	'can_export'          => true,
	'rewrite'             => array( 'slug' => 'stories'),
	'capability_type'     => 'post',
	'supports'            => array(
		'title', 'editor', 'thumbnail',
		)
);

register_post_type( 'story', $args );