<?php
$labels = array(
	'name'                => __( 'Learn', 'eat-wheat' ),
	'singular_name'       => __( 'Learn', 'eat-wheat' ),
	'add_new'             => _x( 'Add New Learn', 'eat-wheat', 'eat-wheat' ),
	'add_new_item'        => __( 'Add New Learn', 'eat-wheat' ),
	'edit_item'           => __( 'Edit Learn', 'eat-wheat' ),
	'new_item'            => __( 'New Learn', 'eat-wheat' ),
	'view_item'           => __( 'View Learn', 'eat-wheat' ),
	'search_items'        => __( 'Search Learn', 'eat-wheat' ),
	'not_found'           => __( 'No Learn found', 'eat-wheat' ),
	'not_found_in_trash'  => __( 'No Learn found in Trash', 'eat-wheat' ),
	'parent_item_colon'   => __( 'Parent Learn:', 'eat-wheat' ),
	'menu_name'           => __( 'Learn', 'eat-wheat' ),
);

$args = array(
	'labels'              => $labels,
	'hierarchical'        => false,
	'taxonomies'          => array(),
	'public'              => true,
	'show_ui'             => true,
	'show_in_menu'        => true,
	'show_in_admin_bar'   => true,
	'menu_position'       => null,
	'menu_icon'           => 'dashicons-edit',
	'show_in_nav_menus'   => true,
	'publicly_queryable'  => true,
	'exclude_from_search' => false,
	'has_archive'         => true,
	'query_var'           => true,
	'can_export'          => true,
	'rewrite'             => array( 'slug' => 'learn'),
	'capability_type'     => 'post',
	'supports'            => array(
		'title', 'editor', 'thumbnail',
		)
);

register_post_type( 'learn', $args );