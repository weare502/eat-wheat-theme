<?php
$labels = array(
	'name'                => __( 'Recipes', 'eat-wheat' ),
	'singular_name'       => __( 'Recipe', 'eat-wheat' ),
	'add_new'             => _x( 'Add New Recipe', 'eat-wheat', 'eat-wheat' ),
	'add_new_item'        => __( 'Add New Recipe', 'eat-wheat' ),
	'edit_item'           => __( 'Edit Recipe', 'eat-wheat' ),
	'new_item'            => __( 'New Recipe', 'eat-wheat' ),
	'view_item'           => __( 'View Recipe', 'eat-wheat' ),
	'search_items'        => __( 'Search Recipes', 'eat-wheat' ),
	'not_found'           => __( 'No Recipes found', 'eat-wheat' ),
	'not_found_in_trash'  => __( 'No Recipes found in Trash', 'eat-wheat' ),
	'parent_item_colon'   => __( 'Parent Recipe:', 'eat-wheat' ),
	'menu_name'           => __( 'Recipes', 'eat-wheat' ),
);

$args = array(
	'labels'                   => $labels,
	'hierarchical'        => false,
	'taxonomies'          => array(),
	'public'              => true,
	'show_ui'             => true,
	'show_in_menu'        => true,
	'show_in_admin_bar'   => true,
	'menu_position'       => null,
	'menu_icon'           => 'dashicons-carrot',
	'show_in_nav_menus'   => true,
	'publicly_queryable'  => true,
	'exclude_from_search' => false,
	'has_archive'         => true,
	'query_var'           => true,
	'can_export'          => true,
	'rewrite'             => array( 'slug' => 'recipes'),
	'capability_type'     => 'post',
	'supports'            => array(
		'title', 'editor', 'thumbnail',
		)
);

register_post_type( 'recipe', $args );


$labels = array(
	'name'                  => _x( 'Meal Types', 'Taxonomy Plural Name', 'eat-wheat' ),
	'singular_name'         => _x( 'Meal Type', 'Taxonomy Singular Name', 'eat-wheat' ),
	'search_items'          => __( 'Search Meal Types', 'eat-wheat' ),
	'popular_items'         => __( 'Popular Meal Types', 'eat-wheat' ),
	'all_items'             => __( 'All Meal Types', 'eat-wheat' ),
	'parent_item'           => __( 'Parent Meal Type', 'eat-wheat' ),
	'parent_item_colon'     => __( 'Parent Meal Type', 'eat-wheat' ),
	'edit_item'             => __( 'Edit Meal Type', 'eat-wheat' ),
	'update_item'           => __( 'Update Meal Type', 'eat-wheat' ),
	'add_new_item'          => __( 'Add New Meal Type', 'eat-wheat' ),
	'new_item_name'         => __( 'New Meal Type Name', 'eat-wheat' ),
	'add_or_remove_items'   => __( 'Add or remove Meal Types', 'eat-wheat' ),
	'choose_from_most_used' => __( 'Choose from most used Meal Types', 'eat-wheat' ),
	'menu_name'             => __( 'Meal Types', 'eat-wheat' ),
);

$args = array(
	'labels'            => $labels,
	'public'            => true,
	'show_in_nav_menus' => true,
	'show_admin_column' => true,
	'hierarchical'      => true,
	'show_tagcloud'     => true,
	'show_ui'           => true,
	'query_var'         => true,
	'rewrite'           => true,
	'query_var'         => true,
	'capabilities'      => array(),
);

register_taxonomy( 'meal-type', array( 'recipe' ), $args );

$labels = array(
	'name'                  => _x( 'Cooking Styles', 'Taxonomy Plural Name', 'eat-wheat' ),
	'singular_name'         => _x( 'Cooking Style', 'Taxonomy Singular Name', 'eat-wheat' ),
	'search_items'          => __( 'Search Cooking Styles', 'eat-wheat' ),
	'popular_items'         => __( 'Popular Cooking Styles', 'eat-wheat' ),
	'all_items'             => __( 'All Cooking Styles', 'eat-wheat' ),
	'parent_item'           => __( 'Parent Cooking Style', 'eat-wheat' ),
	'parent_item_colon'     => __( 'Parent Cooking Style', 'eat-wheat' ),
	'edit_item'             => __( 'Edit Cooking Style', 'eat-wheat' ),
	'update_item'           => __( 'Update Cooking Style', 'eat-wheat' ),
	'add_new_item'          => __( 'Add New Cooking Style', 'eat-wheat' ),
	'new_item_name'         => __( 'New Cooking Style Name', 'eat-wheat' ),
	'add_or_remove_items'   => __( 'Add or remove Cooking Styles', 'eat-wheat' ),
	'choose_from_most_used' => __( 'Choose from most used Cooking Styles', 'eat-wheat' ),
	'menu_name'             => __( 'Cooking Styles', 'eat-wheat' ),
);

$args = array(
	'labels'            => $labels,
	'public'            => true,
	'show_in_nav_menus' => true,
	'show_admin_column' => true,
	'hierarchical'      => true,
	'show_tagcloud'     => true,
	'show_ui'           => true,
	'query_var'         => true,
	'rewrite'           => true,
	'query_var'         => true,
	'capabilities'      => array(),
);

register_taxonomy( 'cooking-style', array( 'recipe' ), $args );