<?php
$labels = array(
	'name'                => __( 'Inspirations', 'eat-wheat' ),
	'singular_name'       => __( 'Inspiration', 'eat-wheat' ),
	'add_new'             => _x( 'Add New Inspiration', 'eat-wheat', 'eat-wheat' ),
	'add_new_item'        => __( 'Add New Inspiration', 'eat-wheat' ),
	'edit_item'           => __( 'Edit Inspiration', 'eat-wheat' ),
	'new_item'            => __( 'New Inspiration', 'eat-wheat' ),
	'view_item'           => __( 'View Inspiration', 'eat-wheat' ),
	'search_items'        => __( 'Search Inspirations', 'eat-wheat' ),
	'not_found'           => __( 'No Inspirations found', 'eat-wheat' ),
	'not_found_in_trash'  => __( 'No Inspirations found in Trash', 'eat-wheat' ),
	'parent_item_colon'   => __( 'Parent Inspiration:', 'eat-wheat' ),
	'menu_name'           => __( 'Inspirations', 'eat-wheat' ),
);

$args = array(
	'labels'              => $labels,
	'hierarchical'        => false,
	'taxonomies'          => array(),
	'public'              => true,
	'show_ui'             => true,
	'show_in_menu'        => true,
	'show_in_admin_bar'   => true,
	'menu_position'       => null,
	'menu_icon'           => 'dashicons-lightbulb',
	'show_in_nav_menus'   => true,
	'publicly_queryable'  => true,
	'exclude_from_search' => false,
	'has_archive'         => true,
	'query_var'           => true,
	'can_export'          => true,
	'rewrite'             => array( 'slug' => 'inspiration'),
	'capability_type'     => 'post',
	'supports'            => array(
		'title', 'editor', 'thumbnail',
		)
);

register_post_type( 'inspiration', $args );